<?php 
/* Array based snippets */

/* Checks if array is multi dimentional */
function is_multi_array( $arr ) {
    rsort( $arr );
    return isset( $arr[0] ) && is_array( $arr[0] );
}