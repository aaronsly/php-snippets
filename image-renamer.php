<?php
 
// change this variable accordingly
$dir = "./unprocessed";
 
// put all files in a directory into an array
if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != "..") {
            $files[] = $file;
        }
    }
    closedir($handle);
}

// strip .jpg from name
$removeArr = [".jpg"," "];
foreach ($files as $file) { 
	$oldname = $file;
	$file = str_replace($removeArr,'',$file);	
	$file = str_replace('-','_',$file);
	$file = str_replace('_S','-S',$file);
	
	$startPos = strpos($file,'_');
	
	if($startPos) {
		$first = explode('_',$file);	
		$last = substr($file,$startPos);	
		$newFileName = strtolower($first[0].$last);
	} else {
		$newFileName = strtolower($file);
	}
	
echo strtolower($newFileName).'<br>';
	
	rename('./unprocessed/'.$oldname, './processed/'.$newFileName . ".jpg");
}
?>