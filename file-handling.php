<?php
/* File Handling Cheat Sheet */

/* Accessing Directory */

//Read Directory Files into Array
$dir = 'example' ;
$files = scandir ( $dir , 'w' ) or die ( 'Cannot read directory: ' . $dir );
$files = array_diff( $files, array( '.','..' ) ); // remove directory links
//implicitly creates file
foreach ( $files as $file ) {
	//do something with $file
	if (substr($file, 0, 1) != '.') { // Dont display files starting with a '.'
		echo $file.'<br>';
	}
}


//Open and Close Directory
$dir = 'example' ;
$dh = opendir ( $dir ) or die ( 'Cannot open file: ' . $file );
//access directory
closedir ( $dh );


//Read Directory
$dir = 'example' ;
if ( $dh = opendir ( $dir )) {
	while (( $file = readdir ( $dh )) !== false ) {
		if ( substr ( $file , 0, 1) !== '.' ) {
			//do something with $file
		}
	}
	closedir ( $dh );
}


/* Accessing Files */

//Create a File
$file = 'file.txt' ;
$fh = fopen ( $file , 'w' ) or die ( 'Cannot open file: ' . $file );
//implicitly creates file
fclose ( $fh );


//Open and Close a File
$file = 'file.txt' ;
$fh = fopen ( $file , 'w' ) or die ( 'Cannot open file: ' . $file );
//open file ('w','r','a')... see mode below
fclose ( $fh );


//Read a File
$file = 'file.txt' ;
$fh = fopen ( $file , 'r' );
$data = fread ( $fh , filesize ( $file ));
fclose ( $fh );


//Write to a File
$file = 'file.txt' ;
$fh = fopen ( $file , 'w' ) or die ( 'Cannot open file: ' . $file );
$data = 'This is the data' ;
fwrite ( $fh , $data );
fclose ( $fh );

//Append to a File
$file = 'file.txt' ;
$fh = fopen ( $file , 'a' ) or die ( 'Cannot open file: ' . $file );
$data = 'New data line 1' ;
fwrite ( $fh , $data );
$new_data = "\n" . 'New data line 2' ;
fwrite ( $fh , $new_data );
fclose ( $fh );


//Delete a File
$file = 'file.txt' ;
unlink ( $file );

// Write line to csv
$array = [];
$fp = fopen('file.csv', 'w');

foreach ($array as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);

/*
File Modes

r Open a file for read only . File pointer starts at the beginning of the file

w Open a file for write only . Erases the contents of the file or creates a new file if it doesn't exist. File pointer starts at the beginning of the file

a Open a file for write only . The existing data in file is preserved. File pointer starts at the end of the file. Creates a new file if the file doesn't exist x Creates a new file for write only. Returns FALSE and an error if file already exists

r+ Open a file for read/write . File pointer starts at the beginning of the file

w+ Open a file for read/write . Erases the contents of the file or creates a new file if it doesn't exist. File pointer starts at the beginning of the file

a+ Open a file for read/write . The existing data in file is preserved. File pointer starts at the end of the file. Creates a new file if the file doesn't exist

x+ Creates a new file for read/write . Returns FALSE and an error if file already exists

*/

// output human readable file sizes
function human_filesize($bytes, $decimals = 2) {
	
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
	
}

// csv to assoc. array
function csvToAssoc($filePath) {

    if( pathinfo( $filePath, PATHINFO_EXTENSION ) != 'csv' || !file_exists($filePath) ) {
        echo("FILE IS NOT A CSV OR DOES NOT EXIST");
        return false;
    }        
	
	$rows = array_map('str_getcsv', file($filePath));
	$header = array_shift($rows);
	$items = [];
	// Process file into assoc. array
	foreach ($rows as $row) {
		$items[] = array_combine($header, $row);
	}

	return $items;
}
