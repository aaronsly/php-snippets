<?php
/*****************************************
* Autoload Classes
*****************************************/
function autoload( $class, $dir = null ) {
 
    if ( is_null( $dir ) )
      $dir = __DIR__.'/classes/'; // can be changed to any directory needed to store classes
 
    foreach ( scandir( $dir ) as $file ) {		
      // directory?
      if ( is_dir( $dir.$file ) && substr( $file, 0, 1 ) !== '.' ) {
		  autoload( $class, $dir.$file.'/' ); 
	  }        
      // php file?
      if ( substr( $file, 0, 2 ) !== '._' && preg_match( "/.php$/i" , $file ) ) { 
        // filename matches class?
        if ( str_replace( '.php', '', $file ) == $class || str_replace( '.class.php', '', $file ) == $class ) {
// 			echo $dir . $file."<br>";
            require_once $dir . $file;
        }
      }
    }	
  }

spl_autoload_register('autoload');

// include wordpress functions
// require_once __DIR__.'/classes/vendor/autoload.php'; // uncomment if composer libs are being used and need to be included