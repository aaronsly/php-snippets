<?php
/* XML Snippets */

// Generate XML from array 
function generateXmlFromArray($array, $node_name) {
	$xml = '';

	if (is_array($array) || is_object($array)) {
		foreach ($array as $key=>$value) {
			if (is_numeric($key)) {
				$key = $node_name;
			}

			$xml .= '<' . $key . '>' . "\n" . generateXmlFromArray($value, $node_name) . '</' . $key . '>' . "\n";
		}
	} else {
		$xml = htmlspecialchars($array, ENT_QUOTES) . "\n";
	}

	return $xml;
	}

function generateValidXmlFromArray($array, $node_block = 'nodes', $node_name = 'node') {
		$xml = '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";

		$xml .= '<' . $node_block . '>' . "\n";
		$xml .= generateXmlFromArray($array, $node_name);
		$xml .= '</' . $node_block . '>' . "\n";

		return $xml;
	}