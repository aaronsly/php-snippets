<?php 
/* Date and Time based snippets */

/* Work out the number of working days between 2 dates */
function number_of_working_days($from, $to) {
    $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
    $holidayDays = ['*-12-25', '*-01-01', '2013-12-23']; # variable and fixed holidays
    $from = new DateTime($from);
    $to = new DateTime($to);
    //$to->modify('+1 day');
    $interval = new DateInterval('P1D');
    $periods = new DatePeriod($from, $interval, $to);
    $days = 0;
    foreach ($periods as $period) {
        if (!in_array($period->format('N'), $workingDays)) continue;
        if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
        if (in_array($period->format('*-m-d'), $holidayDays)) continue;
        $days++;
    }
    return $days;
}

/* get array of future English and Welsh holidays */
function getUkHolidays() {
	$bankHols = json_decode( file_get_contents( 'https://www.gov.uk/bank-holidays.json' ), true );	
	if ( isset( $bankHols ) ) {		
		$dates = array_column( $bankHols['england-and-wales']['events'], 'date' );
//		$dates = array_column( $bankHols['northern-ireland']['events'], 'date' );
//		$dates = array_column( $bankHols['scotland']['events'], 'date' );
		foreach($dates as $date) {
			if( strtotime($date) > strtotime('now') ) {
				$futureDates[] = $date;
			}
		}
		return $futureDates;
	} else {
		return false;
	}	
}