<?php 
/* Debugging Snippets */

/* Display script memory usage */

/*
** Conversion chart for reference **
1 Byte = 8 Bit
1 Kilobyte = 1024 Bytes
1 Megabyte = 1048576 Bytes
1 Gigabyte = 1073741824 Bytes
*/
echo "Memory Usage: " . (memory_get_usage()/1048576) . " MB \n";

/*
** Display error messages on screen **
*/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*
** Output a variable to log file
*  string, int, array, object etc..
*/
$var = 'test';
file_put_contents("test.log", print_r($var,true), FILE_APPEND);

/*
** Calculate script / process time
*/
// Set script / process start time
$time_start = microtime(true);
// Set script / process end time
$time_end = microtime(true);
//dividing by 60 will give the execution time in minutes other wise seconds
$execution_time = number_format( ($time_end - $time_start)/60, 2 );
$execution_time2 = number_format( ($time_end - $time_start), 2 );
//execution time of the script
echo $execution_time.' Mins';
echo '<br>'.$execution_time2.' Seconds';